// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Miller_PaintballGameMode.generated.h"

UCLASS(minimalapi)
class AMiller_PaintballGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMiller_PaintballGameMode();
};




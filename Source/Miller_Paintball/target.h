// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "target.generated.h"

UCLASS()
class MILLER_PAINTBALL_API Atarget : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly)
		class UBoxComponent* CollisionComp;
	
public:	
	// Sets default values for this actor's properties
	Atarget();


	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void PostInitializeComponents() override;
	
		
	UFUNCTION()
		void toggleMaterial();
	
	UPROPERTY(EditAnywhere)
		float Fheight = 2.f;
	UPROPERTY(EditAnywhere)
		float Fwidth = 50.f;
	UPROPERTY(EditAnywhere)
		int IcrossSection = 20;
private:
	void GenerateMesh();
	virtual void CreateCylinder(TArray<FVector>& Vertices, TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FProcMeshTangent>& Tangents, float Height, float Width, int CrossSectionCount);

	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh;
	
	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic * m_dynamicMaterialInstance;

	void turnOffMaterial();
	void turnOnMaterial();
	bool toggle = true;
};

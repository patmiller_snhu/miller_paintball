// Fill out your copyright notice in the Description page of Project Settings.

/*
This actor uses a box component as its rootcomponent and has a static mesh offset from the rootcomponent
That way a unit vector from the root component will have its origin point offset from the static mesh
giving the actor a vector to rotate around
*/
#include "rotating_platform.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/Engine.h"
#include "Math/Vector.h"
#include "Miller_PaintballCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
Arotating_platform::Arotating_platform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	root_box = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));
	RootComponent = root_box;
	
	BoxVisual = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("platform"));
	BoxVisual->SetupAttachment(root_box);
	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshRef(TEXT("StaticMesh'/Game/Geometry/Meshes/moving_platform1.moving_platform1'"));
	if (MeshRef.Succeeded())
	{//Constructs a cube from a static mesh in the engine
		BoxVisual->SetStaticMesh(MeshRef.Object);
		BoxVisual->SetRelativeLocation(FVector(radius, radius, 0.0f));//move the platform away from the root component. The farther away, the larger the path the platform follows
		BoxVisual->SetWorldScale3D(FVector(1.f));
		BoxVisual->SetSimulatePhysics(false);
		BoxVisual->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
		BoxVisual->WakeRigidBody();
		
	}


}

// Called when the game starts or when spawned
void Arotating_platform::BeginPlay(){
	Super::BeginPlay();
	float triangle_leg = sqrt((radius * radius) / 2);//use the pythagorean theorum to determin how far to move the platform from the center
	BoxVisual->SetRelativeLocation(FVector(triangle_leg, triangle_leg, 0.0f));//move the platform away from the root component. The farther away, the larger the path the platform follows
	BoxVisual->SetMaterial(0, Material);//set the material
	can_change_radius = check_can_change_radius();//check if the platform needs to stay at a set radius. Or if it can oscillate in and out
}
// Called every frame
void Arotating_platform::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
	frame_tick += DeltaTime;//at lower frame rates, deltaTime will be greater. Keeping low frame rates from slowing down the platform
	(can_rotate) ?  rotate(frame_tick, DeltaTime) : can_rotate = check_can_move();//if can_rotate is true, just move. if can_rotate is false, see if the platform can start moving
}
void Arotating_platform::rotate(float offset, float delta){
	(can_change_radius) ? spiral(offset, delta) : root_box->AddRelativeRotation(FQuat(FVector(0, 0, 1), (PI/2)  * speed * delta));//if can oscillate, oscillate. if not just rotate
	(offset >= 2 * PI) ? frame_tick = 0.f : NULL;//reset offset once it reaches 2Pi
}
void Arotating_platform::spiral(float offset, float delta){
	BoxVisual->AddRelativeLocation(FVector(radius_offset_amplitued * cos(offset * radius_offset_frequency), radius_offset_amplitued * cos(offset * radius_offset_frequency), 0.f));//oscillate
	root_box->AddRelativeRotation(FQuat(FVector(0, 0, 1), (PI/2) * speed * delta));//rotate
	(offset >= 2 * PI) ? frame_tick = 0.f : NULL;//reset offset once it reaches 2Pi
}
bool Arotating_platform::check_can_move()
{
	//get a reference to the player character
	AMiller_PaintballCharacter *player = Cast<AMiller_PaintballCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (player != NULL) {
		switch (target_group)//check if the target group of this platform is currently active
		{
		case(1):
			return player->target_1_hit;
		case(2):
			return player->target_2_hit;
		case(3):
			return player->target_3_hit;
		default:
			break;
		}
	}
	return false;
}
bool Arotating_platform::check_can_change_radius(){
	return !(FMath::IsNearlyZero(radius_offset_amplitued) & FMath::IsNearlyZero(radius_offset_frequency));//if the oscilation variables are too small, don't oscillate
}


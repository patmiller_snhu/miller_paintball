// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "rotating_platform.generated.h"

UCLASS()
class MILLER_PAINTBALL_API Arotating_platform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Arotating_platform();
	UPROPERTY()
		UStaticMeshComponent* BoxVisual;

	UPROPERTY(EditAnywhere)
		float speed = 0.f;

	UPROPERTY(EditAnywhere)
		int target_group = 0;

	UPROPERTY(EditAnywhere)
		float radius = 100.f;

	UPROPERTY(EditAnywhere)
		float radius_offset_frequency = 0.f;

	UPROPERTY(EditAnywhere)
		float radius_offset_amplitued = 0.f;

	UPROPERTY(EditAnywhere)
		UMaterialInterface * Material;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void rotate(float offset, float delta);
	void spiral(float offset, float delta);
	float frame_tick = 0.f;
	bool check_can_move();
	bool check_can_change_radius();
	UBoxComponent* root_box;

	bool can_rotate = false;
	bool can_change_radius = false;
	

	bool flip_flop = true;
};

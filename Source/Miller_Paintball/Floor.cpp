// Fill out your copyright notice in the Description page of Project Settings.

/*This actor is based on the ProceduralMeshComponent tutorial.
Rather than making a single tile, this actor can create a wall of tiles.
 The number of rows, and columns, as well as the size of the tiles and the size of the gap between the
 tiles, are UPROPERTIES which can be modyfied with the editor*/
#include "Floor.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/MaterialInstanceDynamic.h"

 // Sets default values
AFloor::AFloor() : m_wholeTextureRegion(0, 0, 0, 0, m_textureSize, m_textureSize)
{
	mesh = CreateDefaultSubobject  <UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = mesh;
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//get the material
	static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/material/floor.floor'"));
	m_dynamicMaterial = asset.Object;

	// Create the runtime texture.
	if (!m_dynamicTexture)
	{
		m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8);
		m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		m_dynamicTexture->SRGB = 0;
		m_dynamicTexture->UpdateResource();
		m_dynamicTexture->MipGenSettings = TMGS_NoMipmaps;
	}

	// Initialize array to opaque
	for (int x = 0; x < m_textureSize; ++x)
		for (int y = 0; y < m_textureSize; ++y)
			m_pixelArray[y * m_textureSize + x] = 255;

	// Propagate memory's array to the texture.
	if (m_dynamicTexture)
		m_dynamicTexture->UpdateTextureRegions(0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray);

	if (m_dynamicMaterialInstance)
		mesh->SetMaterial(0, m_dynamicMaterialInstance);


}

// Called when the game starts or when spawned
void AFloor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void  AFloor::PostActorCreated() {
	Super::PostActorCreated();
	CreatePlane(tiles_x, tiles_y);



}   // This is called when actor is already in level and map is opened.  
// Propagate memory's array to the texture.




void  AFloor::PostLoad() {
	Super::PostLoad();
	CreatePlane(tiles_x, tiles_y);
	// Propagate memory's array to the texture.


}

//creates the triangles and vertecies for each tile
void  AFloor::CreateSquare(int y_offset, int z_offset) {

	// add the triangles
	Triangles.Add(Vertices.Num() + 0);
	Triangles.Add(Vertices.Num() + 1);
	Triangles.Add(Vertices.Num() + 2);  // end of triangle 0
	Triangles.Add(Vertices.Num() + 3);
	Triangles.Add(Vertices.Num() + 2);
	Triangles.Add(Vertices.Num() + 1);  // end of triangle 1

	//add the vertecies
	Vertices.Add(FVector(0.f, y_offset * tile_buffer, z_offset * tile_buffer)); //set the bottom left vertex of the current tile
	Vertices.Add(FVector(0.f, (y_offset * tile_buffer) + tile_size, z_offset * tile_buffer));// set the upper left vertex
	Vertices.Add(FVector(0.f, y_offset * tile_buffer, (z_offset * tile_buffer) + tile_size));// set the bottom right vertex
	Vertices.Add(FVector(0.f, (y_offset * tile_buffer) + tile_size, (z_offset * tile_buffer) + tile_size));	//set the upper right

}

void AFloor::CreatePlane(int y, int z) {

	tile_buffer = tile_gap + tile_size; //tile buffer is used to offset the bottom left corner of each tile, so they don't overlap


	//create the tiles one at a time
	//i & j are used to set the relative locations of each new tile
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < z; j++) {
			CreateSquare(i, j);
		}
	}
	// The square lays on a plane, so the normal for all four vertices is the same
	for (int32 i = 0; i < Vertices.Num(); i++) {
		Normals.Add(FVector(0.f, 0.f, 1.f));

		// All vertices get the same color
		Colors.Add(FLinearColor::Red);
	}
	// The following function does the actual creation of the mesh
	mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UV0, Colors, Tangents, true);
}
void AFloor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Create a dynamic material instance 
	if (m_dynamicMaterial)
	{
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
		m_dynamicMaterialInstance->SetTextureParameterValue("texture", m_dynamicTexture);
	}
	
	if (!material_override) {
		// Set the dynamic material to the mesh
		if (m_dynamicMaterialInstance)
			mesh->SetMaterial(0, m_dynamicMaterialInstance);
	}
	else {
		mesh->SetMaterial(0, material_override);
	}
	
}
// Fill out your copyright notice in the Description page of Project Settings.

/*
This actor uses the sin of a frame counter to create smooth forward and backward movement
*/
#include "moving_platform.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Miller_PaintballCharacter.h"
#include "Engine/GameEngine.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
Amoving_platform::Amoving_platform()
{
	//build the platform
	{// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
		PrimaryActorTick.bCanEverTick = true;

		BoxVisual = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("platform"));
		RootComponent = BoxVisual;
		static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshRef(TEXT("StaticMesh'/Game/Geometry/Meshes/moving_platform1.moving_platform1'"));
		if (MeshRef.Succeeded())
		{//Constructs a cube from a static mesh in the engine
			BoxVisual->SetStaticMesh(MeshRef.Object);
			BoxVisual->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
			BoxVisual->SetWorldScale3D(FVector(1.f));
			BoxVisual->SetSimulatePhysics(false);
			BoxVisual->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
			BoxVisual->WakeRigidBody();

		}}

	direction.Normalize();//nomalize the direction vector
}


void Amoving_platform::move(float tick){
	movement = hasDirection ? (direction*(sin(tick * speed))*distance) : (GetActorForwardVector()*(sin(tick * speed))*distance);//if no vector is given, move foward
	BoxVisual->AddRelativeLocation(movement,true);//move the platform in the given direction
}
bool Amoving_platform::check_can_move()
{
	//get a reference to the player character
	AMiller_PaintballCharacter *player = Cast<AMiller_PaintballCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (player != NULL) {
		switch (target_group){//cheeck to see if the target group of this moving platform is one of the currently active moving platforms
		case(1):
			return player->target_1_hit;
		case(2):
			return player->target_2_hit;
		case(3):
			return player->target_3_hit;
		default:
			break;
		}
	}
	return false;
}
// Called when the game starts or when spawned
void Amoving_platform::BeginPlay()
{
	Super::BeginPlay();
	BoxVisual->SetMaterial(0, Material);//set the platform's material
	hasDirection = (direction.IsZero()) ? false : true;//check is a direction vector was given
}
// Called every frame
void Amoving_platform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(check_can_move()){//move if this platform is currently active
		move(frame_tick);//move the platform
		frame_tick += 0.01;//frame tick is used control the movement of the platform.
		frame_tick = fmod(frame_tick, 6.28);//drop the frame tick to 0 once it reaches 6.28
	}
}


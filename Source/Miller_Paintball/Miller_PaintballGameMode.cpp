// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Miller_PaintballGameMode.h"
#include "Miller_PaintballHUD.h"
#include "Miller_PaintballCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMiller_PaintballGameMode::AMiller_PaintballGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMiller_PaintballHUD::StaticClass();
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "moving_platform.generated.h"

UCLASS()
class MILLER_PAINTBALL_API Amoving_platform : public AActor
{
	GENERATED_BODY()
		
	
		

public:	
	// Sets default values for this actor's properties
	Amoving_platform();
	UPROPERTY()
	UStaticMeshComponent* BoxVisual;

	UPROPERTY(EditAnywhere)
		float speed = 0.f;
		
	UPROPERTY(EditAnywhere)
		float distance = 0.f;

	UPROPERTY(EditAnywhere)
		FVector direction;

	UPROPERTY(EditAnywhere)
		int target_group = 0;

	UPROPERTY(EditAnywhere)
		UMaterialInterface * Material;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	float frame_tick = 0.f;
	void move(float tick);
	bool check_can_move();
	bool hasDirection;
	FVector movement;
};

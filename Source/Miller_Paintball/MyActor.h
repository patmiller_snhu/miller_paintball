// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "MyActor.generated.h"
 
UCLASS()
class MILLER_PAINTBALL_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
UFUNCTION(BlueprintCallable)
		void self_destruct();
public:
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void CreateSquare(int, int);
	virtual void CreatePlane(int, int);
	virtual void PostInitializeComponents() override;
private:
	
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh;
	UPROPERTY(EditAnywhere)
		float tile_size = 100.0f;
	UPROPERTY(EditAnywhere)
		float tile_gap;
	UPROPERTY(EditAnywhere)
		int tiles_x = 1;
	UPROPERTY(EditAnywhere)
		int tiles_y = 1;
	TArray <FVector> Vertices;
	TArray <int32> Triangles;
	TArray <FVector> Normals;
	TArray < FLinearColor > Colors;
	TArray <FVector2D> UV0;
	TArray <FProcMeshTangent> Tangents;
	float tile_buffer;
	float Foffset = 0.f;
	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic * m_dynamicMaterialInstance;
	UPROPERTY()
		UTexture2D * m_dynamicTexture;
	
	static const int m_textureSize = 512;
	uint8 m_pixelArray[m_textureSize * m_textureSize]; // Array the size of the texture
	FUpdateTextureRegion2D m_wholeTextureRegion;
	
	
};

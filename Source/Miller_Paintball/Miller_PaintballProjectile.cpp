// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


/*
When this projectile hits a target, it will check the tag on that target and activate a corrisponding boolean on the player character
When said boolean is activated, all moving platforms for that platform group will activate
decals are applied on hit via blueprints
*/
#include "Miller_PaintballProjectile.h"
#include "Miller_PaintballCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "target.h"
#include "moving_platform.h"
#include "Containers/Array.h"
#include "Components/SphereComponent.h"

//default UE4 first person shooter constructer
AMiller_PaintballProjectile::AMiller_PaintballProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AMiller_PaintballProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
	

	
}

void AMiller_PaintballProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Atarget * target = Cast<Atarget>(OtherActor);
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && target !=nullptr) {
		
		//get a reference to the player character
		AMiller_PaintballCharacter *player = Cast<AMiller_PaintballCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		
		if (player != NULL) {
			if (target->Tags.Num() != 0) {
				//check the tag of the target that was hit
				if (target->Tags[0] == FName(TEXT("target_1"))) {

					player->target_1_hit = true;//activate moving platforms in group 1
				}
				else if (target->Tags[0] == FName(TEXT("target_2"))) {

					player->target_2_hit = true;//activate moving platforms in group 2
				}
				else if (target->Tags[0] == FName(TEXT("target_3"))) {

					player->target_3_hit = true;//activate moving targets in group 3
				}
				else {
					//the target doesn't have a valid tag set
					GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "error: " + OtherActor->GetName() + " is set to target group " + target->Tags[0].ToString() + ". This is not a valid target group");
				}
			}
			else {
				//the target doesn't have any tags set
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "error: " + OtherActor->GetName() + " does not have a target group set");
			}
		}
	}
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
		Destroy();
	}
	Destroy();//decal has been applyed by blueprints, make the paintball go splat
}
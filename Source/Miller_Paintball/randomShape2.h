// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RuntimeMeshActor.h"
#include "randomShape2.generated.h"

/**
 * 
 */
UCLASS()
class MILLER_PAINTBALL_API ArandomShape2 : public ARuntimeMeshActor
{
	GENERATED_BODY()

public:
	UMaterialInterface* Material;

public:
	ArandomShape2();

	void OnConstruction(const FTransform& Transform) override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	
	virtual void CreateBoxMesh(FVector BoxRadius,
		TArray<FVector> & Vertices,
		TArray<int32> & Triangles,
		TArray<FVector> & Normals,
		TArray<FVector2D> & UVs,
		TArray<FRuntimeMeshTangent> & Tangents,
		TArray<FColor> & Colors);

	URuntimeMeshProviderStatic* StaticProvider;
	float Foffset = 0.f;
	virtual void CreateBoxMesh(FVector BoxRadius,
		TArray<FVector> & Vertices,
		TArray<int32> & Triangles,
		TArray<FVector> & Normals,
		TArray<FVector2D> & UVs,
		TArray<FRuntimeMeshTangent> & Tangents,
		TArray<FColor> & Colors, float offset);

	
};

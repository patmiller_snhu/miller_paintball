// Fill out your copyright notice in the Description page of Project Settings.


#include "target.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
Atarget::Atarget()
{
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMesh"));
	RootComponent = mesh;
 	

	// Load the base material from the created material.
	static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/material/target.target'"));
	m_dynamicMaterial = asset.Object;
}



void Atarget::PostActorCreated()
{
	Super::PostActorCreated();
	GenerateMesh();

}
//call generate mesh

void Atarget::PostLoad()
{
	Super::PostLoad();
	GenerateMesh();

}
//call generate mesh

void Atarget::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Create a dynamic material instance
	if (m_dynamicMaterial)
	{
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
	}

	// add the dynamic material to the mesh
	if (m_dynamicMaterialInstance)
		mesh->SetMaterial(0, m_dynamicMaterialInstance);
}
//create the dynamic material

//turn off the material's emission (currently unused)
void Atarget::turnOffMaterial()
{
	m_dynamicMaterialInstance->SetScalarParameterValue(TEXT("Emission"), 0);
}

//turn on the material's emission (also unused)
void Atarget::turnOnMaterial()
{
	m_dynamicMaterialInstance->SetScalarParameterValue(TEXT("Emission"), 30);
}

//toggle between turning the material's emission on and off (also unused)
void Atarget::toggleMaterial()
{
	if (toggle) {
		turnOffMaterial();
		toggle = false;
	}
	else {
		turnOnMaterial();
		toggle = true;
	}
}

//create the mesh
void Atarget::GenerateMesh()
{
	
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FProcMeshTangent> Tangents;
	TArray<FColor> VertexColors;
	
	CreateCylinder(Vertices, Triangles, Normals, UVs, Tangents, Fheight, Fwidth, IcrossSection);

	mesh->ClearAllMeshSections();
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, VertexColors, Tangents, true);
	
}
//calculate the verticies, triangles, normals, UVs, and tangents of the cylinder
void Atarget::CreateCylinder(TArray<FVector>& Vertices, TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FProcMeshTangent>& Tangents, float Height, float Width, int CrossSectionCount)
{
	// -------------------------------------------------------
	// Basic setup
	int VertexIndex = 0;
	int32 NumVerts = CrossSectionCount * 4; // InCrossSectionCount x 4 verts per face

	

	// Count vertices for caps
	NumVerts += 2 * (CrossSectionCount - 1) * 3;
	

	// Clear out the arrays passed in
	Triangles.Reset();

	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);

	Normals.Reset();
	Normals.AddUninitialized(NumVerts);

	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

	UVs.Reset();
	UVs.AddUninitialized(NumVerts);

	// -------------------------------------------------------
	// Make a cylinder section
	const float AngleBetweenQuads = (2.0f / (float)(CrossSectionCount)) * PI;
	const float VMapPerQuad = 1.0f / (float)CrossSectionCount;
	FVector Offset = FVector(0, 0, Height);

	// Start by building up vertices that make up the cylinder sides
	for (int32 QuadIndex = 0; QuadIndex < CrossSectionCount; QuadIndex++)
	{
		float Angle = (float)QuadIndex * AngleBetweenQuads;
		float NextAngle = (float)(QuadIndex + 1) * AngleBetweenQuads;

		// Set up the vertices
		FVector p0 = FVector(FMath::Cos(Angle) * Width, FMath::Sin(Angle) * Width, 0.f);
		FVector p1 = FVector(FMath::Cos(NextAngle) * Width, FMath::Sin(NextAngle) * Width, 0.f);
		FVector p2 = p1 + Offset;
		FVector p3 = p0 + Offset;

		// Set up the quad triangles
		int VertIndex1 = VertexIndex++;
		int VertIndex2 = VertexIndex++;
		int VertIndex3 = VertexIndex++;
		int VertIndex4 = VertexIndex++;

		Vertices[VertIndex1] = p0;
		Vertices[VertIndex2] = p1;
		Vertices[VertIndex3] = p2;
		Vertices[VertIndex4] = p3;

		// Now create two triangles from those four vertices
		// The order of these (clockwise/counter-clockwise) dictates which way the normal will face. 
		Triangles.Add(VertIndex4);
		Triangles.Add(VertIndex3);
		Triangles.Add(VertIndex1);

		Triangles.Add(VertIndex3);
		Triangles.Add(VertIndex2);
		Triangles.Add(VertIndex1);

		// UVs
		UVs[VertIndex1] = FVector2D(VMapPerQuad * QuadIndex, 0.0f);
		UVs[VertIndex2] = FVector2D(VMapPerQuad * (QuadIndex + 1), 0.0f);
		UVs[VertIndex3] = FVector2D(VMapPerQuad * (QuadIndex + 1), 1.0f);
		UVs[VertIndex4] = FVector2D(VMapPerQuad * QuadIndex, 1.0f);

		// Normals
		FVector NormalCurrent = FVector::CrossProduct(Vertices[VertIndex1] - Vertices[VertIndex3], Vertices[VertIndex2] - Vertices[VertIndex3]).GetSafeNormal();

		Normals[VertIndex1] = NormalCurrent;
		Normals[VertIndex2] = NormalCurrent;
		Normals[VertIndex3] = NormalCurrent;
		Normals[VertIndex4] = NormalCurrent;
		
		// Tangents (perpendicular to the surface)
		FVector SurfaceTangent = p0 - p1;
		SurfaceTangent = SurfaceTangent.GetSafeNormal();
		Tangents[VertIndex1] = FProcMeshTangent(SurfaceTangent, true);
		Tangents[VertIndex2] = FProcMeshTangent(SurfaceTangent, true);
		Tangents[VertIndex3] = FProcMeshTangent(SurfaceTangent, true);
		Tangents[VertIndex4] = FProcMeshTangent(SurfaceTangent, true);

		

		if (QuadIndex != 0)
		{
			// Cap is closed by triangles that start at 0, then use the points at the angles for the other corners

			// Bottom
			FVector capVertex0 = FVector(FMath::Cos(0) * Width, FMath::Sin(0) * Width, 0.f);
			FVector capVertex1 = FVector(FMath::Cos(Angle) * Width, FMath::Sin(Angle) * Width, 0.f);
			FVector capVertex2 = FVector(FMath::Cos(NextAngle) * Width, FMath::Sin(NextAngle) * Width, 0.f);

			VertIndex1 = VertexIndex++;
			VertIndex2 = VertexIndex++;
			VertIndex3 = VertexIndex++;
			Vertices[VertIndex1] = capVertex0;
			Vertices[VertIndex2] = capVertex1;
			Vertices[VertIndex3] = capVertex2;

			Triangles.Add(VertIndex1);
			Triangles.Add(VertIndex2);
			Triangles.Add(VertIndex3);

			FVector2D UV1 = FVector2D(FMath::Sin(0), FMath::Cos(0));
			FVector2D UV2 = FVector2D(FMath::Sin(Angle), FMath::Cos(Angle));
			FVector2D UV3 = FVector2D(FMath::Sin(NextAngle), FMath::Cos(NextAngle));

			UVs[VertIndex1] = UV1;
			UVs[VertIndex2] = UV2;
			UVs[VertIndex3] = UV3;

			// Top
			capVertex0 = capVertex0 + Offset;
			capVertex1 = capVertex1 + Offset;
			capVertex2 = capVertex2 + Offset;

			VertIndex1 = VertexIndex++;
			VertIndex2 = VertexIndex++;
			VertIndex3 = VertexIndex++;
			Vertices[VertIndex1] = capVertex0;
			Vertices[VertIndex2] = capVertex1;
			Vertices[VertIndex3] = capVertex2;

			Triangles.Add(VertIndex3);
			Triangles.Add(VertIndex2);
			Triangles.Add(VertIndex1);

			UVs[VertIndex1] = UV1;
			UVs[VertIndex2] = UV2;
			UVs[VertIndex3] = UV3;
		}
	}
}

